# Maintainer: Johannes Löthberg <johannes@kyriasis.com>
# Maintainer: Daniel M. Capella <polyzen@archlinux.org>

pkgname=pacman-contrib
pkgver=1.10.6
pkgrel=4
pkgdesc='Contributed scripts and tools for pacman systems'
arch=('x86_64')
url=https://gitlab.archlinux.org/pacman/pacman-contrib
license=('GPL-2.0-or-later')
depends=('pacman')
makedepends=('asciidoc' 'git')
optdepends=(
  'diffutils: for pacdiff'
  'fakeroot: for checkupdates'
  'findutils: for pacdiff --find'
  'mlocate: for pacdiff --locate'
  'perl: for pacsearch'
  'sudo: privilege elevation for several scripts'
  'doas: privilege elevation for several scripts'
  'vim: default merge program for pacdiff'
)
source=("git+$url.git#tag=v$pkgver"
        "0001-lunaos-add-doas-support.patch")
b2sums=('bc84555fb64552b21b3146c91defccecfaa879931720fb4f38eebd37199319409ae4011db5be9b4523a516ea1a2cfa4a07930a00c629e8e905e8d66126f185d0'
        'da59abc9e486c667940d85213317b48d9a4e00f09d30838296c33513d2e373ee65f8bfaac3fe2bff2bfc0eb78b14df2ee232d178184f7a50e0b3325039b2f159')

prepare() {
  cd $pkgname
  patch -Np1 < "../0001-lunaos-add-doas-support.patch"
  ./autogen.sh
}

build() {
  cd $pkgname
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --localstatedir=/var
  make
}

check() {
  cd $pkgname
  make check
}

package() {
  cd $pkgname
  make DESTDIR="$pkgdir" install
}

# vim:set ts=2 sw=2 et:
